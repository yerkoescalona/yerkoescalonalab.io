---
title: Making VIM a powerful IDE
subtitle: tips 
date: 2019-12-25
tags: ["vim", "programming"]
comments: true
---

Rightnow I am a huge fan of the [Vundle plug-in manager](https://github.com/VundleVim/Vundle.vim). It's a very nice plug-in manager of Vim.
Easy to install. Just:

<!--more-->

Follow the instructions of 

```bash
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```

and then you have to configure your ~/.vimrc file using a [template](https://raw.githubusercontent.com/VundleVim/Vundle.vim/master/test/minirc.vim) provided by Vundle and then nstall the plugin by writing *:PluginInstall* or by using the terminal:

```bash
vim +PluginInstall +qall
```



My personal configuration file contains multiple plugins, most of them relate with programming with PYTHON.

A useufl plugin to control the identation of your code is Indent Python: 

 
```
Plugin 'vim-scripts/indentpython.vim'
```

Another useful plugin is flake8, that helps you to follow the rule of PEP: 

```
Plugin 'nvie/vim-flake8'
```

A program that improves the visualization of the status of your editor
```
Plugin 'powerline/powerline'
```


A program that guide you which lines are removed, added or just modified since your last commit is gitglutter
```
Plugin 'airblade/vim-gitgutter'
```

NERDTree
--------


```bash
Plugin 'scrooloose/nerdtree'
```

```bash
nmap <F8> :NERDTreeToggle<CR>
```

Tagbar
------

```
Plugin 'majutsushi/tagbar'
```

```
set tags=./tags;,tags;$HOME
nmap <F9> :TagbarToggle<CR>
nmap <F10> :TagbarOpen<Space>j<CR>
let g:tagbar_autofocus = 1
let g:tagbar_left = 1
```

autotag
-------

```
Plugin 'craigemery/vim-autotag'
```

```
let g:autotagTagsFile="tags"
```

YouCompleteMe
-------------

```vim
Plugin 'Valloric/YouCompleteMe'
```


```markdown
let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>
let g:ycm_global_ycm_extra_conf = '/home/yescalona/.vim/bundle/YouCompleteMe/.ycm_extra_conf.py' 
```

sudo apt install python3-dev
cd ~/.vim/bundle/YouCompleteMe
python3 install.py --clang-completer 

syntastic
--------- 

```vim
Plugin 'vim-syntastic/syntastic'
```

```bash
let g:syntastic_python_python_exec = 'python3'
let g:syntastic_cpp_compiler = 'clang++'
let g:syntastic_cpp_compiler_options = ' -std=c++11 -stdlib=libc++'
let g:syntastic_cpp_check_header = 1
let g:syntastic_python_checkers=['flake8']
let g:syntastic_python_flake8_args='--max-line-length=100'
```

indent guides 
```vim
Plugin 'nathanaelkane/vim-indent-guides'
```


```bash
"let g:indent_guides_enable_on_vim_startup = 1
set ts=2 sw=2 et
let g:indent_guides_start_level = 2
```




