---
title: SLURM in your workstation
subtitle: turn your workstation into a mini-grid with SLURM
date: 2019-12-20
tags: ["linux", "slurm"]
comments: true
---

Turn your workstation into a mini-grid (with Slurm).

<!--more-->

- Install the slurm package using your favourite terminal:


```bash
    sudo apt-get install slurm-wlm
```

- Generate the slurm.conf file

you can create your configuration file using a simple template provided in /usr/share/doc/slurm-client/examples/slurm.conf.simple.gz

```bash
    gunzip /usr/share/doc/slurm-client/examples/slurm.conf.simple.gz
    sudo cp slum.conf.simple /etc/slurm-llnl/slurm.conf
```

you need to configure the following lines:

```bash
...
SlurmctldHost=YOURHOSTNAME
...
NodeName=YOURHOSTNAME CPUs=1 State=UNKNOWN
PartitionName=debug Nodes=YOURHOSTNAME Default=YES MaxTime=INFINITE State=UP

```

- restart the daemons:

```bash
    sudo service slurmd restart
    sudo service slurmctld restart
```

And enjoy your mini-cluster.

- If you need to generate a munge key use this:

```bash
    sudo /usr/sbin/create-munge-key
```

