---
title: VIM 
subtitle: Como correr VIM sin reiniciar tu máquina
date: 2019-12-27
tags: ["linux", "vim"]
comments: true
---

Es común que los principiantes al momento de usar VIM se queden sin poder salir del programa.

<!--more-->

Si inocentemente ejecutaste <kbd>vim</kbd>, tienes que saber que VIM tiene dos modos principales: NORMAL e INSERT MODE. 
Por defecto se entra con el modo NORMAL, en el cual puedes correr comandos específicos de vim. 
Para cambiar al modo INSERT debes tipear <kbd>i</kbd>. El modo INSERT, te permite insertar/escribir texto.
Para volver al modo normal debes apretar <kbd>Esc</kbd>.

En el modo NORMAL se pueden ejecutar differentes comandos:

- <kbd>:w</kbd> escribir
- <kbd>:w filename</kbd> escribir archivo
- <kbd>:q</kbd> salir
- <kbd>:wq</kbd> escribir + salir
- <kbd>:q!</kbd> salir sin escribir
- <kbd>dd</kbd> cortar o borrar linea
- <kbd>yy</kbd> copiar linea
- <kbd>p</kbd> pegar linea
- <kbd>u</kbd> deshacer cambios
- <kbd>\<number\>dd</kbd> cortar o borrar un determinado número de lineas
- <kbd>0</kbd> ir al inicio de la linea
- <kbd>A</kbd> ir al final de la linea
- <kbd>gg</kbd> ir al inicio del archivo 
- <kbd>GG</kbd> ir al final del archivo
- <kbd>/\<blabla\></kbd> buscar algo
- <kbd>n</kbd> ir a la siguiente palabra
- <kbd>N</kbd> ir la palabra previa
- <kbd>:\<line#\></kbd> ir a una linea con un determinado número
- <kbd>\<#\></kbd> salta un determinado número de lineas
- <kbd>%s/foo/bar/g</kbd> buscar <kbd>foo</kbd> y reemplazar por <kbd>bar</kbd> en todo el archivo <kbd>%</kbd> y todas sus ocurrencias <kbd>g</kbd> 

Personalmente me gusta la filosfía de escribir en VIM, donde se separa claramaente los comandos para escribir y ejecutar comandos. Puedo confiar que no agregaré un carácter o número por error en mis programas.


